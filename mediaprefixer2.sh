#!/bin/bash

# ------------------------------------------------------
# undefined input checkpoint
# ------------------------------------------------------
if [ -z "${1}" ]; then
  echo "mediaprefixer2 v0.2a | MIT Licence | https://codeberg.org/jrm-omg"
  echo "renaming your files based on their EXIF (or MODIFY) datetime"
  echo "incredibely useful to organize your own pictures, videos, etc."
  echo ""
  echo "- dry mode usage: mediaprefixer2.sh <input> DRY"
  echo "- single file usage: mediaprefixer2.sh <input>"
  echo "- batch usage : find -maxdepth 1 -type f -exec mediaprefixer2.sh {} \;"
  echo ""
  exit
fi

# ------------------------------------------------------
# PREREQUISITE
# ------------------------------------------------------
# YOU NEED TO INSTALL exiftool FIRST !!!
# YOU NEED TO INSTALL exiftool FIRST !!!
# YOU NEED TO INSTALL exiftool FIRST !!!
# see the installation procedure bellow↓


# ------------------------------------------------------
# EXIFTOOL (yes you need that package)
# ------------------------------------------------------
# For first you need to install [ExifTool](https://exiftool.org/) . So execute the appropriate command based on your distribution.

# Debian/Ubuntu bases:
# sudo apt install libimage-exiftool-perl


# Arch bases:
# sudo pacman -S perl-image-exiftool

# Fedora bases:
# sudo dnf install perl-image-exiftool

# ------------------------------------------------------
# color helpers
# ------------------------------------------------------
green="\033[1;32m"
reset="\033[0m"

# ------------------------------------------------------
# required program checkpoint
# ------------------------------------------------------
command -v exiftool >/dev/null 2>&1 || {
  echo >&2 "I require exiftool but it's not installed.  Aborting. Ciao.";
  exit 1;
}

# ------------------------------------------------------
# input modifier (if using find)
# ------------------------------------------------------
first_chars=$(echo "${1}" | cut -c -2);
if [ $first_chars = "./" ]; then
  # echo "looks like you're using find :B";
  input=$(echo "${1}" | cut -c 3-);
else
  input=$1;
fi

# ------------------------------------------------------
# input filename variables
# ------------------------------------------------------
input_basename="${input##*/}"
input_path=$(dirname "${input}")
input_filename=$(basename "${input%.*}") # oh yeah
input_extension="${input_basename##*.}"
input_extension=${input_extension,,} # lowercase

# ------------------------------------------------------
# input file datetime
# ------------------------------------------------------
exif_used=1
datetime=$(exiftool -dateFormat '%Y%m%d-%H%M%S' -T -datetimeoriginal "$input");
datetime="${datetime//[!0-9-]/}" #filter
if [[ $datetime == '00000000000000' ]] || [[ $datetime == '-' ]] || [[ -z $datetime ]]; then

  # 2nd pass for some video files
  datetime=$(exiftool -dateFormat '%Y%m%d-%H%M%S' -T -createdate "$input");
  datetime="${datetime//[!0-9-]/}" #filter
  if [[ $datetime == '00000000000000' ]] || [[ $datetime == '-' ]] || [[ -z $datetime ]]; then
    # no exif datetime so picking the file modify datetime
    datetime=$(date -r "$input" +%Y%m%d-%H%M%S);
    exif_used=0
  fi
  
fi

# ------------------------------------------------------
# output filename
# ------------------------------------------------------
output_basename="${datetime}--${input_filename}.${input_extension}"
output_fullpath="${input_path}/${output_basename}"

# ------------------------------------------------------
# rename
# ------------------------------------------------------
if [[ "$exif_used" == 1 ]]; then
  dateSourceInfo="${green}(exif)${reset}"
else
  dateSourceInfo="${green}(modified)${reset}"
fi

if test ${2+defined}; then
  echo -e "[DRY] ${green}[in]${reset} ${input_basename} ${green}[out]${reset} ${output_basename} ${dateSourceInfo}"
else
  echo -e "${green}[in]${reset} ${input_basename} ${green}[out]${reset} ${output_basename} ${dateSourceInfo}"
  mv --backup "$input" "$output_fullpath"
fi
