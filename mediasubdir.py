#!/usr/bin/env python3

# run this script inside your working directory (where your files are)
# 
# this will turn this :
# ./20060811-161249--IMG_2934.jpg
# ./20061021-172537--IMG_4506.jpg
# ./20120103-154439--IMG_0428.jpg
# 
# into this :
# ./2006/08/20060811-161249--IMG_2934.jpg
# ./2006/10/20061021-172537--IMG_4506.jpg
# ./2012/12/20120103-154439--IMG_0428.jpg


import sys # args
import os  # path, files
import re  # regex

def mediaPrefixed(str):
    match = re.findall(r'(\d{4})(\d{2})(\d{2})-(\d{6})--(.*)', str)
    if (len(match) > 0):
        res = {}
        res['year']  = match[0][0]
        res['month'] = match[0][1]
        res['day']   = match[0][2]
        res['time']  = match[0][3]
        res['other'] = match[0][4]
        return (res)
    else:
        return False

with os.scandir('.') as it:
    for entry in it:
        if not entry.name.startswith('.') and entry.is_file():
            res = mediaPrefixed(entry.name)
            if res is not False:
                if len(sys.argv) > 1 and sys.argv[1] == 'year=current_dir':
                    path = res['month']
                else:
                    path = res['year']+'/'+res['month']
                os.makedirs(path, exist_ok=True)
                prevFileFP = entry.name
                nextFileFP = path+'/'+entry.name
                if os.path.exists(nextFileFP) is not True:
                    os.rename(prevFileFP, nextFileFP)
                    print('[✔]', prevFileFP, '→', nextFileFP)
                else:
                    print('[x]', nextFileFP, 'already exists !')
            else:
                print('[error] improper format :', entry.name)
