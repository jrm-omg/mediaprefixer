# Rename your pictures and videos

Hi there 👋

I started to have thousands of pictures, videos, screenshots taken from lots of different devices (smartphones, real cameras, drone, etc.) and I thought *what the hell am I going to do with all those files, because they all got different names !* And this is when I started working on those two little scripts :

- `mediaprefixer2.sh`
- `mediasubdir.py`

## Install

1. Download this Repo
2. Run scripts in your terminal

## Demo

### mediaPrefixer

This first script will turn that :

```sh
a4c7614c-16d2169ab10ff74cbbf6b15265be786ca20d16ea.jpeg
DJI_0301.mp4
IMG_3031.dng
IMG_3037.cr3
R0001820.jpg
R0001821.jpg
R0001822.jpg
R0001823.jpg
VID_20160912_110900.mp4
```

Into this :

```sh
20160912-110900--VID_20160912_110900.mp4
20181031-115920--IMG_3031.dng
20181031-115935--IMG_3037.cr3
20191031-115948--DJI_0301.mp4
20201031-115949--R0001820.jpg
20201031-115950--R0001821.jpg
20201031-115955--R0001822.jpg
20201031-120001--R0001823.jpg
20211031-121042--a4c7614c-16d2169ab10ff74cbbf6b15265be786ca20d16ea.jpeg
```

You see the difference ? mediaPrefixer is extracting the EXIF "creation date" for each file, and add that information at the beginning of them. (And if the EXIF header does not exist, mediaPrefixer is using the file modification date).

Pretty useful, doesn't it ?

### mediaSubdir

This second script will help you creating and moving your renamed files (with [mediaPrefixer](#mediaprefixer)) to subfolders.

So for instance, mediaSubdir will move those files, that are stored in the same folder :

```sh
20160912-110900--VID_20160912_110900.mp4
20181031-115920--IMG_3031.dng
20181031-115935--IMG_3037.cr3
20191031-115948--DJI_0301.mp4
20201031-115949--R0001820.jpg
20201031-115950--R0001821.jpg
20201031-115955--R0001822.jpg
20201031-120001--R0001823.jpg
20211031-121042--a4c7614c-16d2169ab10ff74cbbf6b15265be786ca20d16ea.jpeg
```

To this :

```sh
2016/09/
2018/10/
2019/10/
2020/10/
2021/11/
```

Pretty cool, doesn't it ?

## Prerequisite

For first you need to install [ExifTool](https://exiftool.org/) . So execute the appropriate command based on your distribution.


Debian/Ubuntu bases:
```sh
sudo apt install libimage-exiftool-perl
```


Arch bases:
```sh
sudo pacman -S perl-image-exiftool
```

Fedora bases:
```sh
sudo dnf install perl-image-exiftool
```

## mediaPrefixer usage

On a single file :

```sh
mediaprefixer2.sh <filename>
```

On a single file (dry mode, the file won't be renamed) :

```sh
mediaprefixer2.sh <filename> DRY
```

And if you wanna use mediaPrefixer on many files, use `find` :

```sh
find -type f -exec mediaprefixer2.sh {} \;
```

## mediaSubdir usage

The files, from the current directory, WILL BE moved :

```sh
mediasubdir.py
```

## About

Well, I hope you are enjoying that repo as I do !

Feel free to check my [/now](https://humanize.me/now.html) page
